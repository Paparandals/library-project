import BookCover from "./BookCovers.jsx";
import { handleDueDate } from "./Timestamp_handler.js";

const AllLoans = ({ loans, user, handleBookClick }) => {
  function removeDuplicates(loans) {
    let unique = [];
    let isbns = [];
    loans.forEach((loan) => {
      if (!isbns.includes(loan.book_isbn)) {
        isbns.push(loan.book_isbn);
        unique.push(loan);
      }
    });
    return unique;
  }

  return (
    <>
      <div className="currentbox">
        <div className="current-title">{user} is currently reading...</div>
        <div className="current">
          {loans
            .filter((book) => book.returned === null)
            .map((loan) => {
              return (
                <div className="currently" key={"current" + loan.loaned} onClick={() => handleBookClick(loan)}>
                  <div
                    className="currently-cover"
                    onClick={() => handleBookClick(loan)}
                  >
                    <BookCover cover={loan.genre} />
                  </div>
                  <div className="currently-hover">
                    {loan.title} by {loan.author}
                  </div>
                  <div className="due">
                    <div style={{ fontWeight: "bold" }}>Loan due:</div>
                    <div>{`${handleDueDate(loan.due)}`}</div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
      <div className="all-loans">
        <div className="loan-title">All the books {user} has loaned</div>
        <div className="loanbox">
          {removeDuplicates(loans).map((loaned) => {
            return (
              <div className="loans" key={"loanbox" + loaned.loaned}>
                <div
                  className="profile-cover"
                  onClick={() => handleBookClick(loaned)}
                >
                  <BookCover cover={loaned.genre} />
                </div>
                <div className="loans-hover" onClick={() => handleBookClick(loaned)}>
                  {loaned.title} by {loaned.author}
                </div>
              </div>
            );
          })}
        </div>
        <div id="targetbook"></div>
      </div>
    </>
  );
};

export default AllLoans;
