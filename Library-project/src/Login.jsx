import { useState } from "react";
import axios from "axios";
import Logo from "./assets/logo.png"
import "./style/Login.css"

function Login({ setUser, setUserId, setPage }) {
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(null);
    const [logOutMsg, setLogOutMsg] = useState("")
    const [loggedOut, setLoggedOut] = useState(false)

    async function handleSubmit(e) {
        e.preventDefault();
        const url = "https://buutti-library-api.azurewebsites.net/api/users/login";
        const body = { username: name, password: password };

        try {
            const response = await axios.post(url, body);
            const token = response.data.token
            const user = response.data.username
            const id = response.data.id
            localStorage.setItem("token", token)
            localStorage.setItem("user", user)
            localStorage.setItem("id", id)

            setUser(user)
            setUserId(id)
            setSubmitted(true);
            setError(null);
            location.reload()
        } catch (err) {
            setError("Login failed. Please check your credentials.");
        }
        setLoggedOut(false)

    }

    return (
        <div className='log-container'>
            <div className='hatchbox'>Welcome to Book Dragon's Library</div>
            <div className='regbox'>
                <div className='dragon'><img src={Logo} /></div>
                <div className='loginbox'>
                    <div className="login-container">
                        <h2>Log in</h2>
                        <form className="loginform" onSubmit={handleSubmit}>
                            <div className="login-form-group">
                                <label>Username:</label>
                                <input
                                    type="text"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </div>
                            <div className="login-form-group">
                                <label>Password:</label>
                                <input
                                    type="password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>
                            <button type="submit" className="submit-button">Log in</button>
                        </form>
                        {submitted && <p className="success-message">Log in successful!</p>}
                        {error && <p className="error-message">{error}</p>}
                        {loggedOut && <p className="error-message">{logOutMsg}</p>}
                    </div>
                </div>
            </div>

            <div className='no-account-yet'>Don't have an account? <span className='reg-link' onClick={() => setPage("register")}>Register now!</span></div>
        </div>
    );
}

export default Login;