import Children from './assets/children.png'
import Fantasy from './assets/fantasy.png'
import History from './assets/history.png'
import Horror from './assets/horror.png'
import Mystery from './assets/mystery.png'
import Philosophy from './assets/philosophy.png'
import Romance from './assets/romance.png'
import Scifi from './assets/scifi.png'
import Selfhelp from './assets/selfhelp.png'
import Western from './assets/western.png'

const BookCover = (props) => {
    if (props.cover === "Fantasy") {
      return (
        <img src={Fantasy} id="cover"/>
      )
    }
    if (props.cover === "Sci-fi") {
        return (
          <img src={Scifi} id="cover"/>
        )
      }
      if (props.cover === "Self-help") {
        return (
          <img src={Selfhelp} id="cover"/>
        )
      }
      if (props.cover === "Children") {
        return (
          <img src={Children} id="cover"/>
        )
      }
      if (props.cover === "History") {
        return (
          <img src={History} id="cover"/>
        )
      }
      if (props.cover === "Horror") {
        return (
          <img src={Horror} id="cover"/>
        )
      }
      if (props.cover === "Mystery") {
        return (
          <img src={Mystery} id="cover"/>
        )
      }
      if (props.cover === "Philosophy") {
        return (
          <img src={Philosophy} id="cover"/>
        )
      }
      if (props.cover === "Romance") {
        return (
          <img src={Romance} id="cover"/>
        )
      }
      if (props.cover === "Western") {
        return (
          <img src={Western} id="cover"/>
        )
      }
  }

  export default BookCover