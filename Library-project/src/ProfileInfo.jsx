import Icon from './assets/user.png'
import AverageOfUsersReviews from './AverageOfUsersReviews'

const ProfileInfo = ({ loans, user, userReviews }) => {


    return (
        <div className='profilebox'>
            <div className='profile-title'><h3>Profile</h3></div>
            <div className='user'>
                <div className='icon'><img src={Icon} /></div>
                <div className='profile-info'>
                    <div className='profile-username'><h2>{user}</h2></div>
                    <hr className='divider'/>
                    <div className='profile-loans'>{user} has borrowed {loans.length} books since joining!</div>
                    <div><AverageOfUsersReviews userReviews={userReviews} /></div>
                </div>
            </div>
        </div>
    )
}

export default ProfileInfo