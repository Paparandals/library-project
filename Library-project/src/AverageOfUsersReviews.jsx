
const AverageOfUsersReviews = ({ userReviews }) => {

    const rates = userReviews
        .filter(rate => {
            return rate.rating !== 0 || null
        })
    .map(avg => {
        return (
            avg.rating
        )
    })
    let sum = 0
    rates.forEach(number => {
        sum += number
    })
    let average = Math.round((sum / rates.length) * 100) / 100

    return (
        <div>On average they rate their books {average}.</div>
    )
}

export default AverageOfUsersReviews