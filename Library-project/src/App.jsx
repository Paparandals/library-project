import { useState } from "react";
import axios from "axios"
import "./style/App.css";
import Navbar from "./Navbar";
import Register from "./Register";
import AllBooks from "./AllBooks";
import Search from "./Search";
import Login from "./Login"
import Profile from "./profilepage"
import MiniProfile from './MiniProfile'
import TokenCheck from "./TokenCheck";


function App() {
  const [user, setUser] = useState(null)
  const [userId, setUserId] = useState(null)
  const [page, setPage] = useState("home")
  const [allBooks, setAllBooks] = useState([])
  const [isbn, setIsbn] = useState("")
  const [loans, setLoans] = useState([])
  const [allLoans, setAllLoans] = useState([])
  const [userReviews, setUserReviews] = useState([])
  const [open, setOpen] = useState(false);

  TokenCheck(setUser)

  const handleBookClick = (item) => {
    if (item.isbn === undefined) {
      setIsbn(item.book_isbn)
    } else {
      setIsbn(item.isbn)
    }
    setOpen(true)
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  }

  const handleClose = () => {
    setOpen(false);
  }

  const renderPage = () => {
    switch (page) {
      case "home":
        return <AllBooks
          user={user}
          setUser={setUser}
          setUserId={setUserId}
          allBooks={allBooks}
          setAllBooks={setAllBooks}
          isbn={isbn}
          setIsbn={setIsbn}
          handleBookClick={handleBookClick}
          loans={loans}
          setLoans={setLoans}
          userReviews={userReviews}
          setUserReviews={setUserReviews}
          open={open}
          setOpen={setOpen}
          handleClose={handleClose}
          setPage={setPage}
          allLoans={allLoans}
          setAllLoans={setAllLoans}
        />;

      case "search":
        return <Search
          allBooks={allBooks}
          isbn={isbn}
          setIsbn={setIsbn}
          loans={loans}
          setLoans={setLoans}
          userReviews={userReviews}
          setUserReviews={setUserReviews}
          handleBookClick={handleBookClick}
          open={open}
          setOpen={setOpen}
          handleClose={handleClose}
        />;
      case "profile":
        return <Profile
          allBooks={allBooks}
          isbn={isbn}
          setIsbn={setIsbn}
          userReviews={userReviews}
          setUserReviews={setUserReviews}
          loans={loans}
          setLoans={setLoans}
          handleBookClick={handleBookClick}
          open={open}
          setOpen={setOpen}
          handleClose={handleClose} />;
      case "login":
        return <Login
          setUser={setUser}
          setUserId={setUserId}
          setPage={setPage}
        />; //TODO: CHANGE PAGES TO ACTUAL DESIRED ELEMENTS
      case "register":
        return <Register setPage={setPage} />;
      default:
        return <AllBooks
          user={user}
          setUser={setUser}
          setUserId={setUserId}
          allBooks={allBooks}
          setAllBooks={setAllBooks}
          isbn={isbn}
          setIsbn={setIsbn}
          handleBookClick={handleBookClick}
          loans={loans}
          setLoans={setLoans}
          userReviews={userReviews}
          setUserReviews={setUserReviews}
          open={open}
          setOpen={setOpen}
          handleClose={handleClose}
          setPage={setPage}
          allLoans={allLoans}
          setAllLoans={setAllLoans}
        />;
    }
  }

  return (
    <div>
      <Navbar
        page={page}
        setPage={setPage}
        user={user}
        setUser={setUser}
        setUserId={setUserId}
        setIsbn={setIsbn}
      />
      <div className='page'>{renderPage()}</div>
      <div className='can'></div>

    </div>
  );
}

export default App;
