import React from "react";
import "./style/Navbar.css";
import Egg from "./assets/hatch.png"
import { useEffect, useState } from "react"

const Navbar = ({ page, setPage, setUser, setUserId, setIsbn }) => {

  const logout = () => {
    setUser(null)
    setUserId(null)
    setPage("login")
    localStorage.clear()
  }

  const user = localStorage.getItem("user")

  const handleNavbarClick = (pagename) => {
    setPage(pagename)
    pagename === "home" && setIsbn("")
    pagename === "search" && setIsbn("")
    pagename === "profile" && setIsbn("")
  }

  const Login = () => {
    if (user === null) {
      return (
        <>
          <button style={{ backgroundColor: page === "login" ? '#01949A' : '#00436900', }} onClick={() => handleNavbarClick("login")}>Log in
          </button>
          <button style={{ backgroundColor: page === "register" ? '#01949A' : '#00436900', }} onClick={() => handleNavbarClick("register")}>Register
          </button>
        </>
      )
    }

    else {
      return (
        <>
          <button onClick={logout}>Log out</button>
        </>
      )
    }
  }

  return (
    <nav className="navbar">
      <div className='left'>
      <button style={{ backgroundColor: page === "home" ? '#01949A' : '#00436900', }}
        onClick={() => handleNavbarClick("home")}>Home
      </button>
      <button style={{ backgroundColor: page === "search" ? '#01949A' : '#00436900', }}
        onClick={() => handleNavbarClick("search")}>Search
      </button>
      </div>
      <div className='right'>
      {user !== null && <button style={{ backgroundColor: page === "profile" ? '#01949A' : '#00436900', }} onClick={() => handleNavbarClick("profile")}>Profile
      </button>}
      
      <Login />
      </div>

    </nav>
  )
}

export default Navbar;
