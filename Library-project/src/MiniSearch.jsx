
import { useState, useEffect } from 'react'
import Pagination from './Pagination'
import SortOptions from "./SortOptions.jsx"
import compareTitleAscending, {
    compareTitleDescending,
    compareAuthorAscending,
    compareAuthorDescending,
    compareYearAscending,
    compareYearDescending
} from "./Compare_functions.js"
import './style/MiniSearch.css'

const MiniSearch = ({ allBooks, isbn, setIsbn, handleBookClick }) => {

    useEffect(() => {
    }, [])

    const [input, setInput] = useState('')
    const [miniSearchResult, setMiniSearchResult] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [sortBy, setSortBy] = useState("Title")
    const [sortOrder, setSortOrder] = useState("Ascending")
    const [searchClicked, setSearchClicked] = useState(false)

    function removeDuplicates(arr) {
        let unique = []
        arr.forEach(book => {
            if (!unique.includes(book)) {
                unique.push(book)
            }
        })
        return unique
    }


    let tempFilterResult = []

    const handleSearchClick = () => {

        allBooks.filter(book => {
            if (book.isbn.toLowerCase().includes(input.toLowerCase())) {
                tempFilterResult.push(book)
            }
        })
        allBooks.filter(book => {
            if (book.title.toLowerCase().includes(input.toLowerCase())) {
                tempFilterResult.push(book)
            }
        })
        allBooks.filter(book => {
            if (book.author === null) {
                book.author = "Unknown writer"
            }
            if (book.author.toLowerCase().includes(input.toLowerCase())) {
                tempFilterResult.push(book)
            }
        })
        allBooks.filter(book => {
            if (book.genre.toLowerCase().includes(input.toLowerCase())) {
                tempFilterResult.push(book)
            }
        })
        allBooks.filter(book => {
            if (book.year.toString().includes(input)) {
                tempFilterResult.push(book)
            }
        })
        const filteredResult = removeDuplicates(tempFilterResult)
        setSearchClicked(true)
        setMiniSearchResult(filteredResult)
    }

    function sort(source) {

        if (sortBy === "Title" && sortOrder === "Ascending") {
            source.sort(compareTitleAscending)
        }
        if (sortBy === "Title" && sortOrder === "Descending") {
            source.sort(compareTitleDescending)
        }

        if (sortBy === "Author" && sortOrder === "Ascending") {
            source.sort(compareAuthorAscending)
        }
        if (sortBy === "Author" && sortOrder === "Descending") {
            source.sort(compareAuthorDescending)
        }

        if (sortBy === "Year" && sortOrder === "Ascending") {
            source.sort(compareYearAscending)
        }
        if (sortBy === "Year" && sortOrder === "Descending") {
            source.sort(compareYearDescending)
        }
    }

    return (
        <div className="search-wrapper">
            <div className='mini-search'>
                <input type="text"
                    className="mini-search-input"
                    placeholder='Search for a book'
                    value={input}
                    onChange={(event) => setInput(event.target.value)}
                />


                <button type='submit' value='Search' onClick={() => handleSearchClick()} >Search</button>
            </div>

            {(searchClicked === true && miniSearchResult.length === 0) ? <h3>{`Dragon did not find any matches`}</h3> : <></>}
            
            {(searchClicked === true && miniSearchResult.length !== 0) &&
                <>
                    <div className="search-info-sort-container">

                        <h3>{`Dragon found ${miniSearchResult.length} matches`}</h3>
                        <SortOptions
                            setSortBy={setSortBy}
                            setSortOrder={setSortOrder}
                        />

                    </div>

                    <Pagination items={miniSearchResult}
                        currentPage={currentPage}
                        setCurrentPage={setCurrentPage}
                        itemsPerPage={4}
                        sort={sort}
                        handleBookClick={handleBookClick}
                    />
                </>}

            {/* <div id='targetbook'>
                <Book allBooks={allBooks} isbn={isbn} setIsbn={setIsbn} />
            </div> */}

        </div>
    )
}

export default MiniSearch