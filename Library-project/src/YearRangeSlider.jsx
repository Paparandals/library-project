
import { useState } from 'react';
import { Range, getTrackBackground } from 'react-range' //npm install react-range

const YearRangeSlider = ({ search, setSearch }) => {

    const values = [ search.startYear, search.endYear ]

    const handleChange = (values) => {
        setSearch({ ...search, startYear: values[0], endYear: values[1] })
    }


    return (
        <div className="year-range-slider">
            <Range
                values={[search.startYear, search.endYear]}
                step={1}
                min={1900}
                max={2024}
                onChange={handleChange}
                renderTrack={({ props, children }) => (
                    <div
                        {...props}
                        style={{
                            ...props.style,
                            height: '6px',
                            width: '100%',
                            background: getTrackBackground({
                                values: [Number(search.startYear), Number(search.endYear)],
                                colors: ['#fff', '#DB1F48', '#fff'],
                                min: 1900,
                                max: 2024
                            }),
                            borderRadius: '4px',
                            position: 'relative',
                        }}
                    >
                        {children}
                    </div>
                )}
                renderThumb={({ index, props }) => (
                    <div className='slider-ball'
                        {...props}
                        key={props.key}
                        style={{
                            ...props.style,
                        }}
                    >
                        {<div className='year-slider-label'>
                            {values[index]}
                        </div>}
                    </div>
                )}
            />
        </div>
    )
}

export default YearRangeSlider