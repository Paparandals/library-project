
export default function compareTitleAscending(a, b) {
    if (a.title.toLowerCase() < b.title.toLowerCase()) {
        return -1
    }
    if (a.title.toLowerCase() > b.title.toLowerCase()) {
        return 1
    }
    return 0
}

export function compareTitleDescending(a, b) {
    if (a.title.toLowerCase() > b.title.toLowerCase()) {
        return -1
    }
    if (a.title.toLowerCase() < b.title.toLowerCase()) {
        return 1
    }
    return 0
}

export function compareAuthorAscending(a, b) {
    if ((a.author ?? 'default Unknown').toLowerCase() < (b.author ?? 'default Unknown').toLowerCase()) {
        return -1
    }
    if ((a.author ?? 'default Unknown').toLowerCase() > (b.author ?? 'default Unknown').toLowerCase()) {
        return 1
    }
    return 0
}

export function compareAuthorDescending(a, b) {
    if ((a.author ?? 'default Unknown').toLowerCase() > (b.author ?? 'default Unknown').toLowerCase()) {
        return -1
    }
    if ((a.author ?? 'default Unknown').toLowerCase() < (b.author ?? 'default Unknown').toLowerCase()) {
        return 1
    }
    return 0
}

export function compareYearAscending(a, b) {
    if (a.year < b.year) {
        return -1
    }
    if (a.year > b.year) {
        return 1
    }
    return 0
}

export function compareYearDescending(a, b) {
    if (a.year > b.year) {
        return -1
    }
    if (a.year < b.year) {
        return 1
    }
    return 0
}
