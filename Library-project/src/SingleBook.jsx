import { useEffect } from "react";
import BookCover from "./BookCovers";
import AverageOfBooksReviews from "./AverageOfBooksReviews";
import Loan from "./Loan";
import Return from "./Return";
import Extend from "./Extend";
import { handleDueDate, handleReturnedDate } from "./Timestamp_handler.js";

const SingleBook = ({
  allBooks,
  isbn,
  setIsbn,
  loans,
  setLoans,
  reviews,
  latestMatch,
  setLatestMatch,
  getNewestLoanForCurrentIsbn,
  setPage,
}) => {
  const clickedBook = allBooks.filter((book) => {
    return book.isbn === isbn;
  });

  useEffect(() => {}, [isbn]);

  return (
    <div>
      {clickedBook.map((item, i) => {
        return (
          <div className="bookinfo" key={i}>
            <div className="bookpage-cover">
              <BookCover cover={item.genre} />
            </div>
            <div className="info">
              <div>
                <h2>{item.title}</h2>
              </div>
              <hr className="divider" />
              <div className="author">by {item.author}</div>
              <div>Genre: {item.genre}</div>
              <div>Publication year: {item.year}</div>
              <div>
                <AverageOfBooksReviews reviews={reviews} />
              </div>
              <div className="loan-info">
                {latestMatch.returned === null &&
                latestMatch.loaned !== null ? (
                  `Loan due: ${handleDueDate(latestMatch.due)}`
                ) : (
                  <></>
                )}
                {latestMatch.returned !== null &&
                latestMatch.loaned !== null ? (
                  `Returned at: ${handleReturnedDate(latestMatch.returned)}`
                ) : (
                  <></>
                )}
              </div>
              <div className="button-container">
                {(latestMatch.returned !== null ||
                  (latestMatch.returned === null &&
                    latestMatch.due === null)) && (
                  <Loan
                    isbn={isbn}
                    latestMatch={latestMatch}
                    setLatestMatch={setLatestMatch}
                    getNewestLoanForCurrentIsbn={getNewestLoanForCurrentIsbn}
                    loans={loans}
                    setLoans={setLoans}
                    allBooks={allBooks}
                    setPage={setPage}
                  />
                )}
                {latestMatch.returned === null && latestMatch.due !== null && (
                  <Extend
                    isbn={isbn}
                    latestMatch={latestMatch}
                    setLatestMatch={setLatestMatch}
                    getNewestLoanForCurrentIsbn={getNewestLoanForCurrentIsbn}
                  />
                )}
                {latestMatch.returned === null && latestMatch.due !== null && (
                  <Return
                    isbn={isbn}
                    latestMatch={latestMatch}
                    setLatestMatch={setLatestMatch}
                    getNewestLoanForCurrentIsbn={getNewestLoanForCurrentIsbn}
                    loans={loans}
                    setLoans={setLoans}
                  />
                )}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default SingleBook;
