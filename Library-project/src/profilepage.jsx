import { useEffect, useState } from "react"
import axios from "axios"
import './style/Profile.css'
import AllLoans from "./AllLoans"
import ProfileInfo from "./ProfileInfo"
import UserReviews from "./UserReviews"
import Book from "./Book"



function Profile({ allBooks, isbn, setIsbn, loans, setLoans, userReviews, setUserReviews, setOpen, open, handleClose, handleBookClick }) {
    const [user, setUser] = useState(localStorage.getItem("user")) //tämä pitää sitten vetää login-tiedoista

    useEffect(() => {
        initialization()
    }, [])

    async function getAllBooks() {

        const url = 'https://buutti-library-api.azurewebsites.net/api/books'
        const response = await axios.get(url)
        const data = response.data
        return data
    }

    async function getLoans() {

        const url = 'https://buutti-library-api.azurewebsites.net/api/loans'
        const token = localStorage.getItem("token")
        const config = {
            headers: { 'Authorization': 'Bearer ' + token }
        }
        const response = await axios.get(url, config)
        const data = response.data
        return data
    }

    async function getUReviews() {
        const url = 'https://buutti-library-api.azurewebsites.net/api/reviews/'
        const response = await axios.get(url)
        const userReviewList = response.data
        return userReviewList
    }

    async function initialization() {
        const userReviews = await getUReviews()
        const allLoans = await getLoans()
        const books = await getAllBooks()

        const filteredReviews = userReviews.filter(review => {
            return review.username === user
        })

        const reviewsWithNames = filteredReviews.map(review => {
            const bookMatchingReview = books.find(book => book.isbn === review.book_isbn)
            const matches = { ...review, title: bookMatchingReview.title, author: bookMatchingReview.author }
            return matches
        })

        const loanedBooks = allLoans.map(loan => {
            const bookMatchingLoan = books.find(book => book.isbn === loan.book_isbn)
            if (bookMatchingLoan === undefined) {
                return loan
            }
            return { ...loan, title: bookMatchingLoan.title, author: bookMatchingLoan.author, genre: bookMatchingLoan.genre }
        })
        setUserReviews(reviewsWithNames.reverse())
        setLoans(loanedBooks.reverse())
    }

    return (
        <div>
            <div className='Profile'>
                <div>
                    <ProfileInfo
                        loans={loans}
                        user={user}
                        userReviews={userReviews}
                    />
                </div>
                <div className='reading-box'>
                    <AllLoans
                        loans={loans}
                        user={user}
                        setIsbn={setIsbn}
                        handleBookClick={handleBookClick}
                        open={open}
                    />
                </div>
                <div className='user-reviews'>
                    <div className='user-review-title'>
                        <h3>Reviews by {user}</h3>
                    </div>
                    <div className='review'>
                        <UserReviews
                            userReviews={userReviews}
                            setUserReviews={setUserReviews}
                            handleBookClick={handleBookClick}
                            open={open}
                        />
                    </div>
                </div>
                <div id='targetbook'>
                    <Book
                        allBooks={allBooks}
                        isbn={isbn}
                        setIsbn={setIsbn}
                        loans={loans}
                        setLoans={setLoans}
                        open={open}
                        onClose={handleClose}
                        userReviews={userReviews}
                        setUserReviews={setUserReviews}
                    />
                </div>
            </div>
        </div>
    )
}

export default Profile