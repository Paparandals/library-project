import { useEffect, useState } from "react"
import axios from "axios"
import Dragon from './assets/sittingdragon.png'
import './style/Miniprofile.css'

const MiniProfile = ({ user }) => {


    return (
        <div className='mini-profile'>

            <div className='mini-title'>Book Dragon's Library</div>
                <div className='mini-user'>
                    <div className='dragon-logo'><img src={Dragon} /></div>
                    <div className='mini-info'>
                        <h1>Welcome back {user}!</h1>
                        <p>The Book Dragon is delighted to see you again.</p> 
                        <p>What are you going to read next?</p>
                    </div>
            </div>
        </div>
    )
}

export default MiniProfile