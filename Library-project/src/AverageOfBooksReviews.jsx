const AverageOfBooksReviews = ({ reviews }) => {
    const rates = reviews
        .filter(rate => {
            return rate.rating !== "No rating"
        })
        .map(avg => {
            return (
                avg.rating
            )
        })
    let sum = 0
    rates.forEach(number => {
        sum += number
    })
    let average = Math.round((sum / rates.length) * 100) / 100

    if (sum === 0) {
        average = "No ratings yet"
    }

    return (
        <div>Average rating: {average}</div>
    )
}

export default AverageOfBooksReviews