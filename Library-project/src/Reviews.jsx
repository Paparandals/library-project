const Reviews = ({ reviews }) => {

  if (reviews.length === 0) {
    return (
      <div className='review'>No reviews yet</div>
    )
  }

  if (reviews.length > 0) {
    return (
      <div>
        {reviews.map((text, i) => {
          if (text.rating === 0 || null) {
            text.rating = "No rating"
          }
          if (text.review === undefined) {
            text.review = "No review"
          }

          return (
            <div className="review" key={text + i}> 
              <div>
                <div className='review-user'>{text.username}</div>
                <div className='rating'><b>Rating:</b> {text.rating}</div>
                <div className='one-review'><b>Review:</b> {text.review}</div>
              </div>
            </div>
          )
        })}
      </div>
    )

  }

}

export default Reviews