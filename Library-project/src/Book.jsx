import { useEffect, useState } from "react";
import axios from "axios";
import SingleBook from "./SingleBook";
import "./style/Book.css";
import Review from "./Review";
import Reviews from "./Reviews";

function Book({
  allBooks,
  isbn,
  setIsbn,
  open,
  onClose,
  userReviews,
  setUserReviews,
  loans,
  setLoans,
  setPage,
}) {
  const initialLoanStatus = {
    user_id: null,
    book_isbn: null,
    loaned: null,
    returned: null,
    due: null,
  };

  const [latestMatch, setLatestMatch] = useState(initialLoanStatus);
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    getNewestLoanForCurrentIsbn(isbn);
    getReviews(isbn);
  }, [isbn, open]);

  async function getReviews(isbn) {
    const url =
      "https://buutti-library-api.azurewebsites.net/api/reviews/" + isbn;
    const response = await axios.get(url);
    const reviewList = response.data;
    setReviews(reviewList.reverse());
  }

  async function getNewestLoanForCurrentIsbn(isbn) {
    const token = localStorage.getItem("token");
    const url = "https://buutti-library-api.azurewebsites.net/api/loans";
    const config = {
      headers: { Authorization: "Bearer " + token },
    };
    const response = await axios.get(url, config);
    const ownLoans = response.data;
    const currentIsbnLoans = ownLoans.filter((loan) => {
      if (isbn === loan.book_isbn) {
        return loan;
      }
    });

    if (currentIsbnLoans.length === 0) {
      setLatestMatch(initialLoanStatus);
    }

    if (currentIsbnLoans.length > 0) {
      const last = currentIsbnLoans.filter((obj) => {
        return currentIsbnLoans.indexOf(obj) === currentIsbnLoans.length - 1;
      });

      setLatestMatch(last[0]);
    }
  }

  if (open === false) return null;

  return (
    <>
      {isbn !== "" ? (
        <div className="Book" id="targetbook">
          <div className="close">
            <button onClick={onClose}>X</button>
          </div>
          <SingleBook
            allBooks={allBooks}
            isbn={isbn}
            setIsbn={setIsbn}
            loans={loans}
            setLoans={setLoans}
            reviews={reviews}
            latestMatch={latestMatch}
            setLatestMatch={setLatestMatch}
            getNewestLoanForCurrentIsbn={getNewestLoanForCurrentIsbn}
            setPage={setPage}
          />
          {localStorage.getItem("token") !== null ? (
            <div className="review-submit-box">
              <Review
                allBooks={allBooks}
                isbn={isbn}
                reviews={reviews}
                setReviews={setReviews}
                userReviews={userReviews}
                setUserReviews={setUserReviews}
              />
            </div>
          ) : (
            <></>
          )}
          <div className="reviews">
            <h2 className="reviewtitle">Reviews</h2>
            <Reviews reviews={reviews} />
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
}

export default Book;
