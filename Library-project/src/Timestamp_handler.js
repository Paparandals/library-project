
export function handleReturnedDate(input) { // usage on modules: import { handleReturnedDate } from "./Timestamp_handler.js"

    let timestamp_ms = Number(input) + 10800000
    let date = new Date(timestamp_ms)
    return date.toUTCString() + "+3"
}


export function handleDueDate(input) { // usage on modules: import { handleDueDate } from "./Timestamp_handler.js"

    let timestamp_ms = Number(input) - 3600000 + 10800000 + 1209600000 // loan time 2 weeks
    let date = new Date(timestamp_ms)
    return date.toUTCString() + "+3"
}


export default function handleTimestamp(input) { // usage on modules: import handleTimestamp from "./Timestamp_handler.js"

    let timestamp_ms = Number(input) + 10800000
    let date = new Date(timestamp_ms)
    return date.toUTCString() + "+3"
}

