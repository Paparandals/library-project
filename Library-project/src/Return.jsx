import axios from "axios";
import { handleReturnedDate } from "./Timestamp_handler.js";

function Return({
  isbn,
  latestMatch,
  getNewestLoanForCurrentIsbn,
  loans,
  setLoans,
}) {
  async function returnBook() {
    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: "Bearer " + token,
        },
      };
      const url =
        "https://buutti-library-api.azurewebsites.net/api/loans/" + isbn;
      const response = await axios.put(url, {}, config);
      const returnInfo = response.data;
      await getNewestLoanForCurrentIsbn(isbn);
      console.log(
        "book returned, isbn: ",
        isbn,
        "book returned:",
        handleReturnedDate(latestMatch.returned)
      );

      const targetIndex = loans.indexOf(
        loans.find((loan) => loan.book_isbn === isbn && loan.returned === null)
      );
      setLoans((old) => {
        return old.filter((_, i) => i !== targetIndex);
      });
    } catch (error) {
      alert(error);
    }
  }

  return (
    <>
      <button className="return-button" onClick={returnBook}>
        Return
      </button>
    </>
  );
}

export default Return;
