import axios from "axios";
import { useState } from "react";


const UserReviews = ({ userReviews, setUserReviews, handleBookClick }) => {
    const [error, setError] = useState(null)

    async function deleteReview(revId) {
        const url = "https://buutti-library-api.azurewebsites.net/api/reviews/" + revId
        const token = localStorage.getItem("token")
        const config = {
            headers: { "Authorization": "Bearer " + token }
        }
        try {
            const response = await axios.delete(url, config)
            if (response.status === 200) {
                setUserReviews(revs => revs.filter(rev => rev.id !== revId))
                setError(null)
            }
        } catch (err) {
            setError("Deleting review failed. Please try again.")
        }
    }

    return (
        <div>
            {userReviews.map((revs) => {

                if (revs.rating === 0 || null) {
                    revs.rating = "No rating"
                }
                if (revs.review === null || undefined) {
                    revs.review = "No review"
                }

                return (
                    <div key={revs.id} className='review'>
                        <div className='user-review-book' onClick={() => handleBookClick(revs)}>{revs.title} by {revs.author}</div>
                        <div className="review-content-wrapper">
                            <div className="review-content">
                                <div className='userratings'><span style={{ fontWeight: 'bold' }}>Rating:</span> {revs.rating}</div>
                                <div><span style={{ fontWeight: 'bold' }}>Review:</span> {revs.review}</div>
                            </div>

                            <button className="delete-review-button" onClick={() => deleteReview(revs.id)} >Delete</button>
                        </div>

                        {error && <p className="error-message">{error}</p>}
                    </div>)
            })}
        </div>
    )
}

export default UserReviews