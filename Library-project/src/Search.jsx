
import { useEffect, useState } from "react"
import SearchForm from "./SearchForm"
import Pagination from "./Pagination"
import SortOptions from "./SortOptions.jsx"
import Book from "./Book.jsx"
import compareTitleAscending, {
  compareTitleDescending,
  compareAuthorAscending,
  compareAuthorDescending,
  compareYearAscending,
  compareYearDescending
} from "./Compare_functions.js"
import './style/Search.css'

function Search({ allBooks, isbn, setIsbn, handleBookClick, setOpen, open, handleClose, loans, setLoans, userReviews, setUserReviews }) {

  const initialState = {
    isbn: '',
    title: '',
    author: '',
    genre: '',
    genreCheckBoxes: [null, null, null, null, null, null, null, null, null, null],
    startYear: 1900,
    endYear: 2024
  }

  const [search, setSearch] = useState(initialState)
  const [searchResult, setSearchResult] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [searchClicked, setSearchClicked] = useState(false)

  const [sortBy, setSortBy] = useState("Title")
  const [sortOrder, setSortOrder] = useState("Ascending")

  const [children, setChildren] = useState(false)
  const [fantasy, setFantasy] = useState(false)
  const [history, setHistory] = useState(false)
  const [horror, setHorror] = useState(false)
  const [mystery, setMystery] = useState(false)
  const [philosophy, setPhilosophy] = useState(false)
  const [romance, setRomance] = useState(false)
  const [scifi, setScifi] = useState(false)
  const [selfhelp, setSelfhelp] = useState(false)
  const [western, setWestern] = useState(false)

  useEffect(() => {
  }, [searchResult])



  const intersect = (Arr1, Arr2) => {
    let setA = new Set(Arr1)
    let setB = new Set(Arr2)
    const intersection = new Set([...setA].filter(item => setB.has(item)))
    return Array.from(intersection)
  }

  const handleResult = () => {

    if (filterResult.length !== 0) {
      filterResult = intersect(tempFilterResult, filterResult)
      tempFilterResult = []
    }
  }

  let tempFilterResult = []
  let filterResult = allBooks

  const handleSearchSubmit = async (event) => {

    event.preventDefault()
    setCurrentPage(1)

    if (search.isbn.length > 0) {
      allBooks.filter(book => {
        if (book.isbn.toLowerCase().includes(search.isbn.toLowerCase())) {
          tempFilterResult.push(book)
        }
      })
      handleResult()
    }

    if (search.title.length > 0) {
      allBooks.filter(book => {
        if (book.title.toLowerCase().includes(search.title.toLowerCase())) {
          tempFilterResult.push(book)
        }
      })
      handleResult()
    }

    if (search.author.length > 0) {
      allBooks.filter(book => {
        if (book.author === null) {
          book.author = "Unknown writer"
        }
        if (book.author.toLowerCase().includes(search.author.toLowerCase())) {
          tempFilterResult.push(book)
        }
      })
      handleResult()
    }

    if (search.genre.length > 0) {
      allBooks.filter(book => {
        if (book.genre.toLowerCase().includes(search.genre.toLowerCase())) {
          tempFilterResult.push(book)
        }
      })
      handleResult()
    }

    const checkBoxes = []
    search.genreCheckBoxes.map((check) => {
      if (check !== null) {
        checkBoxes.push(check)
      }
    })

    if (checkBoxes.length > 0) {
      for (let i = 0; i < checkBoxes.length; i++) {
        allBooks.filter(book => {
          if (book.genre.toLowerCase().includes(checkBoxes[i].toLowerCase())) {
            tempFilterResult.push(book)
          }
        })
      }
      handleResult()
    }

    allBooks.filter(book => {
      if (book.year >= search.startYear && book.year <= search.endYear) {
        tempFilterResult.push(book)
      }
    })
    handleResult()
    setSearchClicked(true)
    setSearchResult(filterResult)
  }

  function sort(source) {

    if (sortBy === "Title" && sortOrder === "Ascending") {
      source.sort(compareTitleAscending)
    }
    if (sortBy === "Title" && sortOrder === "Descending") {
      source.sort(compareTitleDescending)
    }

    if (sortBy === "Author" && sortOrder === "Ascending") {
      source.sort(compareAuthorAscending)
    }
    if (sortBy === "Author" && sortOrder === "Descending") {
      source.sort(compareAuthorDescending)
    }

    if (sortBy === "Year" && sortOrder === "Ascending") {
      source.sort(compareYearAscending)
    }
    if (sortBy === "Year" && sortOrder === "Descending") {
      source.sort(compareYearDescending)
    }
  }

  return (
    <div className="search-wrapper">
      <SearchForm search={search}
        setIsbn={setIsbn}
        setSearch={setSearch}
        setSearchResult={setSearchResult}
        handleSearchSubmit={handleSearchSubmit}
        initialState={initialState}
        setCurrenPage={setCurrentPage}
        setSearchClicked={setSearchClicked}

        children={children} setChildren={setChildren}
        fantasy={fantasy} setFantasy={setFantasy}
        history={history} setHistory={setHistory}
        horror={horror} setHorror={setHorror}
        mystery={mystery} setMystery={setMystery}
        philosophy={philosophy} setPhilosophy={setPhilosophy}
        romance={romance} setRomance={setRomance}
        scifi={scifi} setScifi={setScifi}
        selfhelp={selfhelp} setSelfhelp={setSelfhelp}
        western={western} setWestern={setWestern}
      />

      {searchResult.length > 0 &&
        <>
          <div className="search-info-sort-container">
            <h3>{`Dragon found ${searchResult.length} matches`}</h3>
            <SortOptions
              setSortBy={setSortBy}
              setSortOrder={setSortOrder}
            />
          </div>
          <Pagination items={searchResult}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            itemsPerPage={10}
            sort={sort}
            handleBookClick={handleBookClick}
          />
        </>
      }
      {(searchClicked === true && searchResult.length === 0) ? <h3>{`Dragon did not find any matches`}</h3> : <></>}
      <div id='targetbook'>
        <Book
          allBooks={allBooks}
          isbn={isbn}
          setIsbn={setIsbn}
          loans={loans}
          setLoans={setLoans}
          userReviews={userReviews}
          setUserReviews={setUserReviews}
          open={open}
          onClose={handleClose}
        />
      </div>

    </div>
  )
}

export default Search