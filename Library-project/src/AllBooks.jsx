import { useEffect, useState, Suspense, lazy } from "react"
import axios from "axios"
import Book from "./Book"
import Search from "./Search"
const Recommendations = lazy(() => import("./Recommendations"))
// import Recommendations from "./Recommendations"
import Profile from "./Profilepage"
import MiniSearch from "./MiniSearch"
import MiniProfile from "./MiniProfile"
import Login from "./Login"
// import Review from "./Review"

function AllBooks({
    user,
    setUser,
    setUserId,
    allBooks,
    setAllBooks,
    isbn,
    setIsbn,
    handleBookClick,
    loans,
    setLoans,
    userReviews,
    setUserReviews,
    open,
    setOpen,
    handleClose,
    setPage,
    allLoans,
    setAllLoans
}) {

    const [randomBooks, setRandomBooks] = useState([]);

    // const deferredRandomBooks = useDeferredValue(randomBooks)

    useEffect(() => {
        setUser(localStorage.getItem("user"))
        getAllBooks()
        getAllLoans()
    }, [])

    async function getAllBooks() {
        // console.log("allBooks haki kirjat")
        const url = 'https://buutti-library-api.azurewebsites.net/api/books'
        const response = await axios.get(url)
        const data = response.data
        setAllBooks([...data])
    }

    async function getAllLoans() {
        // console.log("allBooks haki lainat")
        const url = 'https://buutti-library-api.azurewebsites.net/api/loans'
        const token = localStorage.getItem("token")
        const config = {
            headers: { 'Authorization': 'Bearer ' + token }
        }
        const response = await axios.get(url, config)
        const data = response.data
        setAllLoans(data)
    }

    return (
        <>
            <div className="miniprofile">
                {user === null && <Login setUser={setUser} setUserId={setUserId} setPage={setPage} />}
                {user !== null && <MiniProfile user={user} />
                }
            </div>

            <div className="minisearch">
                <MiniSearch
                    allBooks={allBooks}
                    isbn={isbn}
                    setIsbn={setIsbn}
                    handleBookClick={handleBookClick}
                />
            </div>

            <div>
                <Suspense>
                    <Recommendations
                        user={user}
                        allBooks={allBooks}
                        setIsbn={setIsbn}
                        handleBookClick={handleBookClick}
                        allLoans={allLoans}
                        setAllLoans={setAllLoans}
                        randomBooks={randomBooks}
                        setRandomBooks={setRandomBooks}
                    />
                </Suspense>
            </div>

            <div id='targetbook'>
                <Book
                    allBooks={allBooks}
                    isbn={isbn}
                    setIsbn={setIsbn}
                    loans={loans}
                    setLoans={setLoans}
                    open={open}
                    onClose={handleClose}
                    userReviews={userReviews}
                    setUserReviews={setUserReviews}
                    setPage={setPage}
                />
            </div>

        </>
    )
}

export default AllBooks