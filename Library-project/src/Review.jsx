import React from "react";
import { useState } from "react";
import axios from "axios";
import Quill from "./assets/review.png"
import("./style/Review.css")

function Review({ allBooks, isbn, reviews, setReviews, userReviews, setUserReviews }) {
    const [review, setReview] = useState("")
    const [rating, setRating] = useState("")
    const [error, setError] = useState(null)
    // const [submitted, setSubmitted] = useState(false)
    const user = localStorage.getItem("user")

    async function postReview(e) {
        e.preventDefault();
        const url = "https://buutti-library-api.azurewebsites.net/api/reviews/" + isbn
        const ratingNumber = Number(rating)
        const token = localStorage.getItem("token")
        const body = { review: review, rating: ratingNumber }
        const config = {
            headers: { "Authorization": "Bearer " + token }
        }
        try {
            const response = await axios.post(url, body, config)
            setReviews([
            {
                book_isbn: response.data.book,
                id: response.data.id,
                rating: response.data.rating,
                review: response.data.review,
                user_id: response.data.user,
                username: user
            },
                ...reviews
            ])

            const target = allBooks.find(book => book.isbn === isbn)
            setUserReviews([
            {
                author: target.author,
                title: target.title,
                book_isbn: response.data.book,
                id: response.data.id,
                rating: response.data.rating,
                review: response.data.review,
                user_id: response.data.user,
                username: user
            },
                ...userReviews
            ])

            setReview("")
            setRating("")

        } catch (err) {
            setError("Review failed. Please try again.")
            alert(err)
        }
    }

    return (
        <div className='send-review-container'>
            <div className='critic-title'>Share your thoughts...</div>
            <div className='reviewbox'>
                <div className='quill'><img src={Quill} /></div>
                <div className="review-container">

                    <form onSubmit={postReview} className="review-form">
                        <div className="form-group">
                            <label>Write a review:</label>
                            <input type="text" name="review" value={review} onChange={(event) => setReview(event.target.value)} />
                            <label>Give a rating (1-5):</label>
                            <input type="number" name="rating" min={1} max={5} value={rating} onChange={(event) => setRating(event.target.value)} />
                        </div>
                        <input type="submit" value="Submit" className="submit-button" />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Review;