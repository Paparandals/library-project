import axios from "axios"
import { handleDueDate } from "./Timestamp_handler.js"

function Extend({ isbn, latestMatch, getNewestLoanForCurrentIsbn }) {


    async function extendLoan() {
        try {
            const token = localStorage.getItem("token")
            const config = {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }
            const url = 'https://buutti-library-api.azurewebsites.net/api/loans/' + isbn
            const response = await axios.patch(url, {}, config)
            const loanInfo = response.data
            await getNewestLoanForCurrentIsbn(isbn)
            console.log("loan extended, isbn: ", isbn, "book due:", handleDueDate(latestMatch.due))
        }

        catch (error) {
            alert(error.response.data);
        }
    }

    return (
        <>
            <button className="loan-button" onClick={extendLoan}>Extend</button>
            
        </>
    )
}

export default Extend