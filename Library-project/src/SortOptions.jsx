
const SortOptions = ({
    // searchResult,
    // setSearchResult,
    setSortBy,
    setSortOrder
}) => {

    const sortByHandler = (event) => {
        setSortBy(event.target.value)

    }

    const sortOrderHandler = (event) => {
        setSortOrder(event.target.value)

    }

    return (
        <div className="sort-selection-container">

            <label htmlFor="sortBy">Sort by
                <select name="sortBy"
                    id="sort-by"
                    onChange={(event) => sortByHandler(event)}
                >
                    <option value="Title">Title</option>
                    <option value="Author">Author</option>
                    <option value="Year">Year</option>
                </select>
            </label>

            <label htmlFor="sortOrder">Sort order
                <select name="sortByAuthor"
                    id="sort-by-author"
                    onChange={(event) => sortOrderHandler(event)}
                >
                    <option value="Ascending">Ascending</option>
                    <option value="Descending">Descending</option>
                </select>
            </label>

        </div>
    )
}

export default SortOptions