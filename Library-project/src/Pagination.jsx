
import BookCover from './BookCovers'

const Pagination = ({ items, itemsPerPage, currentPage, setCurrentPage, sort, handleBookClick }) => {

    sort(items)
    const totalPages = Math.ceil(items.length / itemsPerPage)

    const startIndex = (currentPage - 1) * itemsPerPage
    const endIndex = startIndex + itemsPerPage
    const currentItems = items.slice(startIndex, endIndex)

    const handlePageChange = (page) => {
        setCurrentPage(page)
    }


    const getPageNumbers = () => {
        const maxButtons = 9
        let startPage = Math.max(1, currentPage - Math.floor(maxButtons / 2))
        let endPage = startPage + maxButtons - 1

        if (endPage > totalPages) {
            endPage = totalPages
            startPage = Math.max(1, endPage - maxButtons + 1)
        }

        const pages = [];
        for (let i = startPage; i <= endPage; i++) {
            pages.push(i)
        }
        return pages
    }

    // const handleBookClick = (item) => {
    //     console.log(item.isbn, item.title, item.author)
    //     setIsbn(item.isbn)
    //     const target = document.getElementById('targetbook')
    //     target && target.scrollIntoView()
    // }

    const pageNumbers = getPageNumbers()

    return (
        <div className="search-results-container">
            <div className='book-items-wrapper'>
                {currentItems.map((item, index) => (
                    <div className="search-single-book" key={item.isbn} onClick={() => handleBookClick(item)}>
                        <div className='bookcover'><BookCover cover={item.genre} /></div>
                        <div className='booktitle'><p className='hover' style={{fontWeight:"bold"}}>{item.title}</p>   
                        <p>AUTHOR: {item.author} </p>  
                        <p>GENRE: {item.genre} </p>
                        <p>YEAR: {item.year}</p>
                        </div>
                    </div>
                ))}
            </div>

            {/* Pagination Controls */}
            <div className="pagination-controls">

                <button
                    onClick={() => handlePageChange(1)}
                    disabled={currentPage === 1}>First
                </button>

                <button
                    onClick={() => handlePageChange(currentPage - 1)}
                    disabled={currentPage === 1}>Previous
                </button>

                {pageNumbers.map(page => (
                    <button
                        key={page}
                        onClick={() => handlePageChange(page)}
                        className={page === currentPage ? 'active' : ''}>{page}
                    </button>
                ))}

                <button
                    onClick={() => handlePageChange(currentPage + 1)}
                    disabled={currentPage === totalPages}>Next
                </button>

                <button
                    onClick={() => handlePageChange(totalPages)}
                    disabled={currentPage === totalPages}>Last
                </button>

            </div>
        </div>
    )
}

export default Pagination