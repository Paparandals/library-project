import axios from "axios";
import { handleDueDate } from "./Timestamp_handler.js";

function Loan({ isbn, latestMatch, getNewestLoanForCurrentIsbn, loans, setLoans, allBooks, setPage }) {

  async function loanBook() {
    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: "Bearer " + token,
        },
      };
      const url =
        "https://buutti-library-api.azurewebsites.net/api/loans/" + isbn;
      const response = await axios.post(url, {}, config);
      const loanInfo = response.data;
      await getNewestLoanForCurrentIsbn(isbn);
      console.log(
        "book loaned, isbn: ", isbn,
        "book due:", handleDueDate(latestMatch.due)
      );
      const target = allBooks.find(book => book.isbn === isbn)
      setLoans([...loans, {
        author: target.author,
        title: target.title,
        genre: target.genre,
        book_isbn: loanInfo.book,
        due: loanInfo.due,
        loaned: loanInfo.loaned,
        returned: null,
        user_id: loanInfo.user
      } ])
      

    } catch (error) {
      if (error.response.data !== undefined ) {
        alert(error.response.data)
      }
      else {
        alert(error)
      }
    }
  }



  function tokenCheck() {
    const token = localStorage.getItem("token");

    return token !== null ? "Loan" : "Log in to loan"; //todo: make "log in to loan" redirect to login page
  }
  return (
    <>
      <button className="loan-button" 
      onClick={() => 
        localStorage.getItem("token") !== null ? loanBook() : setPage("login")
      }
    >
      {tokenCheck()}
      </button>
    </>
  );
}

export default Loan;