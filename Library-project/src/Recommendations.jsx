import React, { useState, useEffect } from 'react'
import axios from "axios"
import './style/Recommendations.css'
import BookCover from './BookCovers'

const Recommendations = ({ user, allBooks, setIsbn, handleBookClick, allLoans, setAllLoans, randomBooks, setRandomBooks }) => {

  useEffect(() => {
    generateRecommendedBooks()
  }, [allBooks, allLoans])


  // shuffle-funktio kirjoille. Jos antaa count parametrin, 
  // funktio palauttaa kyseisen pituisen sekoitetun listan
  const randomInt = (k) => Math.floor((k + 1) * Math.random())
  const fisherYatesShuffle = (arr, count) => {
    for (let i = arr.length - 1; i > 0; i--) {
      const j = randomInt(i);
      [arr[i], arr[j]] = [arr[j], arr[i]]
    }
    if (count === 0) {
      return arr
    } else {
      return (arr.slice(0, count))
    }
  }

  // funktio etsii listan moodin eli yleisimmän jäsenen
  function mode(input) {
    let mode = {}
    let maxCount = 0
    let modes = []
    input.forEach(function (e) {
      if (mode[e] == null) {
        mode[e] = 1
      } else {
        mode[e]++
      }
      if (mode[e] > maxCount) {
        modes = [e]
        maxCount = mode[e]
      } else if (mode[e] === maxCount) {
        modes.push(e)
      }
    })
    return modes
  }

  function generateRecommendedBooks() {

    // jos riippuvaisuudet ehditään ajamaan, ajetaan tämä haara
    if (allBooks.length > 0 && allLoans.length > 0) {

      const bookMatches = (allLoans.map(loan => allBooks.find(book => book.isbn === loan.book_isbn)))
      let genres = []
      bookMatches.forEach((book) => genres.push(book.genre))

      const modeOfGenres = mode(genres)

      const booksOfGenres = allBooks.filter(book => {
        for (let i = 0; i < genres.length; i++) {
          if (book.genre === modeOfGenres[i]) {
            return book
          }
        }
      })

      const fifteenBooksOfGenres = fisherYatesShuffle(booksOfGenres, 15)
      const fifteenRandomBooks = fisherYatesShuffle(allBooks, 15)
      const recommendedArray = fifteenBooksOfGenres.concat(fifteenRandomBooks)

      setRandomBooks(fisherYatesShuffle(recommendedArray, 0))
    }
    // jos riippuvaisuuksissa kestää, ajetaan simppelimpi versio
    else {
      setRandomBooks(fisherYatesShuffle(allBooks, 30))
    }
  }

  // scroller-animaation js-osuus
  const scrollers = document.querySelectorAll(".recommendations-container")
  if (!window.matchMedia("(prefers-reduced-motion: reduce)").matches) {
    addAnimation()
  }
  function addAnimation() {
    scrollers.forEach(scroller => {
      scroller.setAttribute('data-animated', true)
    })
  }

  // renderit
  return (
    <>
      <div className='recommendationsbox'>
        <div className='recommendations-title'>Dragon's recommendations for you</div>
        <div className='recommendations-container'>
          <div className='recommendations-container-inner'>
            {randomBooks.map((book, index) => (
              <label className='rec-book-item' key={book.isbn + index}>
                <div className='rec-cover-wrapper'>
                  <div className='rec-cover' onClick={() => handleBookClick(book)}><BookCover cover={book.genre} /></div>
                </div>
                <div className="recommendation-item-title" key={index} onClick={() => handleBookClick(book)}>{book.title}</div>
              </label>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

export default Recommendations