import axios from "axios"

async function TokenCheck( setUser, setUserId ) {

    try {
        const url = 'https://buutti-library-api.azurewebsites.net/api/loans'
        const token = localStorage.getItem("token")
        const config = {
            headers: { 'Authorization': 'Bearer ' + token }
        }
        const response = await axios.get(url, config)
    }
    catch (error) {
        if (error.status > 400) {
            setUser(null)
            setUserId(null)
            localStorage.clear()
        }
    }
}

export default TokenCheck