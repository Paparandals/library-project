import { useState } from "react"
import axios from "axios"
import "./style/Register.css";
import Egg from './assets/hatch.png'

function Register( { setPage } ) {
  const [name, setName] = useState("")
  const [password, setPassword] = useState("")
  const [password2, setPassword2] = useState("")
  const [submitted, setSubmitted] = useState(false)
  const [error, setError] = useState(null)

  async function handleSubmit(e) {
    if (password !== password2) {
      alert("Passwords didn't match! Please try again.")
      return
    }
    e.preventDefault()
    const url = "https://buutti-library-api.azurewebsites.net/api/users/register"
    const body = { username: name, password: password }
    try {
      const response = await axios.post(url, body)
      const token = response.data.token
      const user = response.data.username
      const id = response.data.id
      localStorage.setItem("token", token)
      localStorage.setItem("user", user)
      localStorage.setItem("id", id)
      setName("")
      setPassword("")
      setPassword2("")
      setSubmitted(true)
      console.log(response.status)
    } catch (err) {
      setError("Username already exists")
    }
  }
  return (

    <div className='container'>
      <div className='hatchbox'>A new book dragon is about to hatch...</div>
      <div className='regbox'>
        <div className='egg'><img src={Egg} /></div>
        <div className="registration-container">
          <h2>Create an account</h2>
          <form className="regform" onSubmit={handleSubmit}>
            <div className="register-form-group">
              <label>Name:</label>
              <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="register-form-group">
              <label>Password:</label>
              <input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <label>Re-enter password:</label>
              <input
                type="password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
              />
            </div>
            <button type="submit" className="submit-button">Register</button>
          </form>
          {submitted && <p className="success-message">Registration successful!</p>}
          {error && <p className="error-message">{error}</p>}
        </div>
      </div>
      <div className='no-account-yet'>Already a member? <span className='reg-link' onClick={() => setPage("login")}>Log in!</span></div>
    </div>
  );
};

export default Register