
import YearRangeSlider from "./YearRangeSlider"

function SearchForm({
    setIsbn,
    search,
    setSearch,
    setSearchResult,
    handleSearchSubmit,
    initialState,
    setCurrenPage,
    setSearchClicked,
    children, setChildren,
    fantasy, setFantasy,
    history, setHistory,
    horror, setHorror,
    mystery, setMystery,
    philosophy, setPhilosophy,
    romance, setRomance,
    scifi, setScifi,
    selfhelp, setSelfhelp,
    western, setWestern
}) {

    const resetForm = () => {
        setIsbn('')
        setSearch(initialState)
        setSearchClicked(false)
        setSearchResult([])
        setCurrenPage(1)
        setChildren(false)
        setFantasy(false)
        setHistory(false)
        setHorror(false)
        setMystery(false)
        setPhilosophy(false)
        setRomance(false)
        setScifi(false)
        setSelfhelp(false)
        setWestern(false)
    }

    const handleChangeChildren = () => {
        setChildren(!children)
        if (search.genreCheckBoxes[0] === null) {
            setSearch( search, search.genreCheckBoxes[0] = "children" )
        }
        else {
            setSearch( search, search.genreCheckBoxes[0] = null )
        }
    }
    const handleChangeFantasy = () => {
        setFantasy(!fantasy)
        if (search.genreCheckBoxes[1] === null) {
            setSearch( search, search.genreCheckBoxes[1] = "fantasy" )
        }
        else {
            setSearch( search, search.genreCheckBoxes[1] = null )
        }
    }
    const handleChangeHistory = () => {
        setHistory(!history)
        if (search.genreCheckBoxes[2] === null) {
            setSearch( search, search.genreCheckBoxes[2] = "history" )
        }
        else {
            setSearch( search, search.genreCheckBoxes[2] = null )
        }
    }
    const handleChangeHorror = () => {
        setHorror(!horror)
        if (search.genreCheckBoxes[3] === null) {
            setSearch( search, search.genreCheckBoxes[3] = "horror" )
        }
        else {
            setSearch( search, search.genreCheckBoxes[3] = null )
        }
    }
    const handleChangeMystery = () => {
        setMystery(!mystery)
        if (search.genreCheckBoxes[4] === null) {
            setSearch( search, search.genreCheckBoxes[4] = "mystery" )
        }
        else {
            setSearch( search, search.genreCheckBoxes[4] = null )
        }
    }
    const handleChangePhilosophy = () => {
        setPhilosophy(!philosophy)
        if (search.genreCheckBoxes[5] === null) {
            setSearch( search, search.genreCheckBoxes[5] = "philosophy" )
        }
        else {
            setSearch( search, search.genreCheckBoxes[5] = null )
        }
    }
    const handleChangeRomance = () => {
        setRomance(!romance)
        if (search.genreCheckBoxes[6] === null) {
            setSearch( search, search.genreCheckBoxes[6] = "romance" )        
        }
        else {
            setSearch( search, search.genreCheckBoxes[6] = null )
        }
    }
    const handleChangeScifi = () => {
        setScifi(!scifi)
        if (search.genreCheckBoxes[7] === null) {
            setSearch( search, search.genreCheckBoxes[7] = "sci-fi" )      
        }
        else {
            setSearch( search, search.genreCheckBoxes[7] = null )
        }
    }
    const handleChangeSelfhelp = () => {
        setSelfhelp(!selfhelp)
        if (search.genreCheckBoxes[8] === null) {
            setSearch( search, search.genreCheckBoxes[8] = "self-help" )     
        }
        else {
            setSearch( search, search.genreCheckBoxes[8] = null )
        }
    }
    const handleChangeWestern = () => {
        setWestern(!western)
        if (search.genreCheckBoxes[9] === null) {
            setSearch( search, search.genreCheckBoxes[9] = "western" )   
        }
        else {
            setSearch( search, search.genreCheckBoxes[9] = null )
        }
    }


    return (
        <div className="search-form-wrapper">

            <form className="search-form" onSubmit={handleSearchSubmit} onReset={resetForm}>
                <h1>NEED A BOOK? SEARCH HERE.</h1>
                <label>ISBN
                    <input type="text"
                        className="input-isbn"
                        value={search.isbn}
                        onChange={(event) => setSearch({ ...search, isbn: event.target.value })}
                    />
                </label>

                <label>Title
                    <input type="text"
                        className="input-title"
                        value={search.title}
                        onChange={(event) => setSearch({ ...search, title: event.target.value })}
                    />
                </label>

                <label>Author
                    <input type="text"
                        className="input-author"
                        value={search.author}
                        onChange={(event) => setSearch({ ...search, author: event.target.value })}
                    />
                </label>

                <label>Genre
                    <input type="text"
                        className="input-genre"
                        value={search.genre}
                        onChange={(event) => setSearch({ ...search, genre: event.target.value })}
                    />
                </label>

                <label>Genres
                    <div className="genre-checkbox-container">
                        <label htmlFor="Children">
                            <input type="checkbox" value="Children"
                                onChange={() => handleChangeChildren()}
                            /><p>Children</p></label>
                        <label htmlFor="Fantasy">
                            <input type="checkbox" value="Fantasy"
                                onChange={() => handleChangeFantasy()}
                            /><p>Fantasy</p></label>
                        <label htmlFor="History">
                            <input type="checkbox" value="History"
                                onChange={() => handleChangeHistory()}
                            /><p>History</p></label>
                        <label htmlFor="Horror">
                            <input type="checkbox" value="Horror"
                                onChange={() => handleChangeHorror()}
                            /><p>Horror</p></label>
                        <label htmlFor="Mystery">
                            <input type="checkbox" value="Mystery"
                                onChange={() => handleChangeMystery()}
                            /><p>Mystery</p></label>
                        <label htmlFor="Philosophy">
                            <input type="checkbox" value="Philosophy"
                                onChange={() => handleChangePhilosophy()}
                            /><p>Philosophy</p></label>
                        <label htmlFor="Romance">
                            <input type="checkbox" value="Romance"
                                onChange={() => handleChangeRomance()}
                            /><p>Romance</p></label>
                        <label htmlFor="Sci-fi">
                            <input type="checkbox" value="Sci-fi"
                                onChange={() => handleChangeScifi()}
                            /><p>Sci-fi</p></label>
                        <label htmlFor="Self-help">
                            <input type="checkbox" value="Self-help"
                                onChange={() => handleChangeSelfhelp()}
                            /><p>Self-help</p></label>
                        <label htmlFor="Western">
                            <input type="checkbox" value="Western"
                                onChange={() => handleChangeWestern()}
                            /><p>Western</p></label>
                    </div>
                </label>

                <label>Year range
                    <YearRangeSlider search={search} setSearch={setSearch} />
                </label>

                <div className="search-form-button-container">
                    <input type='reset' value='Reset' />
                    <input type='submit' value='Search' />
                </div>

            </form>

        </div>
    )
}

export default SearchForm